"""Tests for simulator.py

Copyright William Clarke, University of Oxford, 2021
"""
import pytest
from fsl_mrs.denmatsim.simulator import simulator
import numpy as np


@pytest.fixture
def spin_sys_singlet():
    """A single singlet peak at 0 ppm."""
    return {
        "j": [[0]],
        "shifts": [0.0],
        "name": "single",
        "scaleFactor": 1}


@pytest.fixture
def sim_obj_singlet(spin_sys_singlet):
    return simulator(spin_sys_singlet, 3.0)


def test_initial_state(sim_obj_singlet: simulator):
    assert np.allclose(sim_obj_singlet.p, np.array([[0.5, 0.0], [0.0, -0.5]]))


def test_rf_hamiltonian(sim_obj_singlet: simulator):
    pulse90 = np.array([250.0, ])
    p = sim_obj_singlet.applyRF(pulse90, 0.001)
    assert np.allclose(p, np.array([[0.0, 0.0 + .5j], [0.0 - .5j, 0.0]]))

    p = sim_obj_singlet.applyRF(pulse90, 0.001, p=p)
    assert np.allclose(p, np.array([[-0.5, 0.0], [0.0, 0.5]]))

    p = sim_obj_singlet.applyRF(pulse90, 0.001, p=p)
    assert np.allclose(p, np.array([[0.0, 0.0 - .5j], [0.0 + .5j, 0.0]]))

    p = sim_obj_singlet.applyRF(pulse90, 0.001, p=p)
    assert np.allclose(sim_obj_singlet.p, np.array([[0.5, 0.0], [0.0, -0.5]]))

    pulse180 = np.array([500.0, ])
    p = sim_obj_singlet.applyRF(pulse180, 0.001, p=sim_obj_singlet.thermalEq())
    assert np.allclose(p, np.array([[-0.5, 0.0], [0.0, 0.5]]))


def test_rf_hamiltonian_with_grad(sim_obj_singlet: simulator):

    pulse90 = 250.0 * np.ones((10,))
    p = sim_obj_singlet.applyRF(pulse90, 0.0001, G=np.array([1.0E-3, 0.0, 0.0]), r=np.array([0.0, 0.0, 0.0]))
    assert np.allclose(p, np.array([[0.0, 0.0 + .5j], [0.0 - .5j, 0.0]]))

    g_array = np.tile(np.array([1.0E-3, 0.0, 0.0]), (10, 1)).T
    p = sim_obj_singlet.applyRF(pulse90, 0.0001, G=g_array, r=np.array([0.0, 0.0, 0.0]), p=sim_obj_singlet.thermalEq())
    assert np.allclose(p, np.array([[0.0, 0.0 + .5j], [0.0 - .5j, 0.0]]))
