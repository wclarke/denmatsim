"""Tests for the simseq module

Copyright Will Clarke, University of Oxford, 2021
"""
import pytest
import numpy as np

from fsl_mrs.denmatsim import simseq


# Arrange RF, delay, rephase, cfilter
def RF(g_axis):
    rng = np.random.default_rng(seed=42)
    grad = np.zeros((3,))
    grad[g_axis] = 1.0
    points = 100
    return {'amp': 100 * rng.random(points),
            'phase': 2 * np.pi * rng.random(points),
            'time': 0.001,
            'grad': grad,
            'frequencyOffset': -100,
            'phaseOffset': 0.0}


@pytest.fixture
def RF_0():
    return RF(0)


@pytest.fixture
def RF_1():
    return RF(1)


@pytest.fixture
def RF_2():
    return RF(2)


@pytest.fixture
def delay():
    return 1E-3


def rephase(g_axis):
    reph = np.zeros((3,))
    reph[g_axis] = 0.001 * 0.5 * 1.0
    return reph


@pytest.fixture
def rp_0():
    return rephase(0)


@pytest.fixture
def rp_1():
    return rephase(1)


@pytest.fixture
def rp_2():
    return rephase(2)


@pytest.fixture
def cf():
    return -1


def test_simblock(RF_0, delay, rp_0, cf):
    block = simseq.simBlock(RF_0, delay, rp_0, cf, GUnits='mT')

    cmplx = RF_0['amp'] * np.exp(1j * RF_0['phase'])

    assert np.allclose(block.getPulse(), cmplx)
    assert block.cfilter == cf
    assert np.allclose(block.sliceGrad, RF_0['grad'] / 1E3)
    assert block.axis == 0


@pytest.fixture
def seqblock_0(RF_0, delay, rp_0, cf):
    return simseq.simBlock(RF_0, delay, rp_0, cf, GUnits='mT')


@pytest.fixture
def seqblock_1(RF_1, delay, rp_1, cf):
    return simseq.simBlock(RF_1, delay, rp_1, cf, GUnits='mT')


@pytest.fixture
def seqblock_2(RF_2, delay, rp_2, cf):
    return simseq.simBlock(RF_2, delay, rp_2, cf, GUnits='mT')


@pytest.fixture
def seqblocks_012(seqblock_0, seqblock_1, seqblock_2):
    return [seqblock_0, seqblock_1, seqblock_2]


@pytest.fixture
def seqblocks_102(seqblock_0, seqblock_1, seqblock_2):
    return [seqblock_1, seqblock_0, seqblock_2]


@pytest.fixture
def seqblocks_0012(seqblock_0, seqblock_1, seqblock_2):
    return [seqblock_0, seqblock_0, seqblock_1, seqblock_2]


@pytest.fixture
def seqblocks_01122(seqblock_0, seqblock_1, seqblock_2):
    return [seqblock_0, seqblock_1, seqblock_1, seqblock_2, seqblock_2]


@pytest.fixture
def seqblocks_01221(seqblock_0, seqblock_1, seqblock_2):
    return [seqblock_0, seqblock_1, seqblock_2, seqblock_2, seqblock_1]


def test_gradOrderChecker(seqblocks_012,
                          seqblocks_102,
                          seqblocks_0012,
                          seqblocks_01122,
                          seqblocks_01221):

    assert simseq.gradOrderChecker(seqblocks_012)[0] == '1d'
    assert simseq.gradOrderChecker(seqblocks_012)[1].tolist() == [0, 1, 2]

    assert simseq.gradOrderChecker(seqblocks_102)[0] == '1d'
    assert simseq.gradOrderChecker(seqblocks_102)[1].tolist() == [1, 0, 2]

    assert simseq.gradOrderChecker(seqblocks_0012)[0] == '1d'
    assert simseq.gradOrderChecker(seqblocks_0012)[1].tolist() == [0, 0, 1, 2]

    assert simseq.gradOrderChecker(seqblocks_01122)[0] == '1d'
    assert simseq.gradOrderChecker(seqblocks_01122)[1].tolist() == [0, 1, 1, 2, 2]

    assert simseq.gradOrderChecker(seqblocks_01122)[0] == '1d'
    assert simseq.gradOrderChecker(seqblocks_01122)[1].tolist() == [0, 1, 1, 2, 2]

    assert simseq.gradOrderChecker(seqblocks_01221)[0] == 'interleaved'
    assert simseq.gradOrderChecker(seqblocks_01221)[1].tolist() == [0, 1, 2, 2, 1]

    seqblocks_012[0].axis = np.asarray([0, 1])
    assert simseq.gradOrderChecker(seqblocks_012)[0] == 'full'
    assert simseq.gradOrderChecker(seqblocks_012)[1].tolist() == []
